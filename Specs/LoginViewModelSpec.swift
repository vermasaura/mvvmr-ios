//
//  LoginViewModelSpec.swift
//  mvvmrTests
//
//  Created by Saurabh Verma on 4/9/18.
//  Copyright © 2018 Outware. All rights reserved.
//

import Quick
import Nimble

@testable import mvvmr

class LoginViewModelSpec: QuickSpec {

  override func spec() {

    describe("a view model for login screen") {

      var router: LoginRouterSpy!
      var viewModel: LoginViewModelType!

      beforeEach {
        router = LoginRouterSpy()
        viewModel = LoginViewModel(router: router)
      }

      afterEach {
        router = nil
        viewModel = nil
      }

      context("on login success") {

        it("presents home screen") {

          viewModel.processloginButtonAction()
          expect(router.timesHomeScreenCalled).to(equal(1))

        }

      }

      context("when forgot password button is tapped") {

        it("presents forgot password screen") {

          viewModel.processForgotPasswordButtonAction()
          expect(router.timesForgotPasswordScreenCalled).to(equal(1))

        }

      }

      context("when signup button is tapped") {

        it("presents signup screen") {

          viewModel.processSignUpButtonAction()
          expect(router.timesSignUpScreenCalled).to(equal(1))

        }

      }

    }

  }

}

private class LoginRouterSpy: LoginRouterType {

  var timesSignUpScreenCalled = 0
  var timesHomeScreenCalled = 0
  var timesForgotPasswordScreenCalled = 0

  // MARK: - LoginRouterType Conformance

  func presentSignupScreen() {
    timesSignUpScreenCalled += 1
  }

  func presentHomeScreen() {
    timesHomeScreenCalled += 1
  }

  func presentForgotPasswordScreen() {
    timesForgotPasswordScreenCalled += 1
  }

}
