//
//  HomeScreenViewController.swift
//  mvvmr
//
//  Created by Saurabh Verma on 4/9/18.
//  Copyright © 2018 Outware. All rights reserved.
//

import UIKit

final class HomeScreenViewController: UIViewController {

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: "HomeScreenViewController", bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    self.title = "HOME"
  }

}
