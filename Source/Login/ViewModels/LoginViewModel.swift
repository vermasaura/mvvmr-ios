//
//  LoginViewModel.swift
//  mvvmr
//
//  Created by Saurabh Verma on 4/9/18.
//  Copyright © 2018 Outware. All rights reserved.
//

final class LoginViewModel: LoginViewModelType {

  // MARK: - Initialization

  init(router: LoginRouterType) {
    self.router = router
  }

  // MARK: LoginViewModelType Conformance

  func processloginButtonAction() {
    router.presentHomeScreen()
  }

  func processSignUpButtonAction() {
    router.presentSignupScreen()
  }

  func processForgotPasswordButtonAction() {
    router.presentForgotPasswordScreen()
  }

  // MARK: - Private

  // MARK: Properties

  private let router: LoginRouterType

}
