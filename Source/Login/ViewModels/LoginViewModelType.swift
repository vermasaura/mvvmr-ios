//
//  LoginViewModelType.swift
//  mvvmr
//
//  Created by Saurabh Verma on 4/9/18.
//  Copyright © 2018 Outware. All rights reserved.
//

protocol LoginViewModelType {

  func processloginButtonAction()

  func processSignUpButtonAction()

  func processForgotPasswordButtonAction()

}
