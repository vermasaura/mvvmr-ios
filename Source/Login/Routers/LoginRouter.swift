//
//  LoginRouter.swift
//  mvvmr
//
//  Created by Saurabh Verma on 4/9/18.
//  Copyright © 2018 Outware. All rights reserved.
//

final class LoginRouter: LoginRouterType {

  // MARK: - LoginRouterType Conformance

  func presentForgotPasswordScreen() {

    let viewController = ForgotPasswordViewController()
    hostViewController?.navigationController?.pushViewController(viewController, animated: true)

  }

  func presentHomeScreen() {

    let viewController = HomeScreenViewController()
    hostViewController?.navigationController?.pushViewController(viewController, animated: true)

  }

  func presentSignupScreen() {

    let viewController = SignUpViewController()
    hostViewController?.navigationController?.pushViewController(viewController, animated: true)

  }

  // MARK: - Private

  // MARK: Properties

  weak var hostViewController: LoginViewController?

}
