//
//  LoginRouterType.swift
//  mvvmr
//
//  Created by Saurabh Verma on 4/9/18.
//  Copyright © 2018 Outware. All rights reserved.
//

protocol LoginRouterType {

  func presentSignupScreen()

  func presentHomeScreen()

  func presentForgotPasswordScreen()

}
