//
//  LoginViewController.swift
//  mvvmr
//
//  Created by Saurabh Verma on 4/9/18.
//  Copyright © 2018 Outware. All rights reserved.
//

import UIKit

final class LoginViewController: UIViewController {

  // MARK: - Initialization

  init(viewModel: LoginViewModelType) {
    self.viewModel = viewModel

    super.init(nibName: "LoginViewController", bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("use init(vieWModel:) instead")
  }

  // MARK: Lifecycle

  override func viewDidLoad() {
    self.title = "LOG IN"
  }

  // MARK: - Private

  // MARK: Functions

  @IBAction private func loginButtonAction(sender: UIButton) {
    viewModel.processloginButtonAction()
  }

  @IBAction private func signUpButtonAction(sender: UIButton) {
    viewModel.processSignUpButtonAction()
  }

  @IBAction private func forgotPasswordButtonAction(sender: UIButton) {
    viewModel.processForgotPasswordButtonAction()
  }

  // MARK: Properties

  private let viewModel: LoginViewModelType

}
