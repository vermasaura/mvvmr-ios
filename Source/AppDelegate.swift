//
//  AppDelegate.swift
//  mvvmr
//
//  Created by Saurabh Verma on 4/9/18.
//  Copyright © 2018 Outware. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

    let router = LoginRouter()
    let viewModel = LoginViewModel(router: router)
    let viewController = LoginViewController(viewModel: viewModel)
    router.hostViewController = viewController

    let navigationController = UINavigationController(rootViewController: viewController)

    window?.rootViewController = navigationController

    return true
  }

}

